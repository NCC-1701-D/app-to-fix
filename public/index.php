<?php
declare(strict_types=1);

use App\Application;
use App\Utilities\Logger\LoggerFactory;

require __DIR__ . '/../vendor/autoload.php';

$logger = (new LoggerFactory())->createLogger();
$mustache_engine = new Mustache_Engine([
    'loader' => new Mustache_Loader_FilesystemLoader('../views'),
]);

try {
    $application = new Application();
    echo $mustache_engine->render('index', ['repositories' => $application->handle()]);
} catch (\Throwable $e) {
    $logger->error($e->getMessage());
    echo $mustache_engine->render('error', ['error' => $e->getMessage()]);
}
