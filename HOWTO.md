# Kanban board for Github issues

## Prerequisites
1. [Docker](https://docs.docker.com/engine/install/)
2. [Docker Compose](https://docs.docker.com/compose/install/)
3. Active account on github
4. [Registered an OAuth application](https://github.com/settings/applications/new)
    - **Homepage URL** and **Authorization callback URL** should be the same. For example for the local development you should set them to:

            http://localhost:8001  

## Setting up
1. Set up environment variables:

        cp .env.dist .env
        
    and then you have to set variables in .env file:

        GH_CLIENT_ID       - you will receive it after registering the application
        GH_CLIENT_SECRET   - you will receive it after registering the application
        GH_ACCOUNT         - the account from which the repositories will be displayed(for example: doctrine)
        GH_REPOSITORIES    - the repositories you want to display, separated by a |(for example: orm|DoctrineMongoDBBundlee)
        
    the **USER_GLOBAL_ID** is set to 1001 by you can change it according to result of the command:
    
        echo $UID
        
    in your host machine(**not in the container**).
    
2. Run the docker container:

        docker-compose up -d --build
        
3. Log in to the php-fpm container:

        docker exec -it php-fpm /bin/bash
        
4. Install dependencies(in container):

        composer install
        
5. The application is available at http://localhost:8001  

## Notices
There is not any asset management system. If you want to update your local .css files you need to copy them to the public directory.     
 
##  To run the tests(in container):
You will use the command:
                    
    ./vendor/phpunit/phpunit/phpunit tests/ --colors
    
## To run the Code Sniffer(in container):
You will use the command:

    ./vendor/squizlabs/php_codesniffer/bin/phpcs --standard=phpcs.xml src/

## To run the PHPStan(in container):
You will use the command:

    ./vendor/phpstan/phpstan/phpstan analyse -c phpstan-config.neon

## Logs
The log files are available in the **/var/www/html/var/logs** directory in the container or in the **var/logs** directory in the project directory.

## TODO
- increase test coverage

## How to test deployed application
The deployed application is available at http://safe-stream-73328.herokuapp.com/

The application is connected to the repositories:

    doctrine/orm
    doctrine/DoctrineMongoDBBundle

You can compare what the application displays and what is available here:
    
    https://github.com/doctrine/orm/milestones
    https://github.com/doctrine/DoctrineMongoDBBundle/milestones
