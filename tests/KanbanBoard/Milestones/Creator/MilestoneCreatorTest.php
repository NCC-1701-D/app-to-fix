<?php
declare(strict_types=1);

namespace App\Tests\KanbanBoard\Milestones\Creator;

use App\KanbanBoard\Milestones\Creator\MilestoneCreator;
use PHPUnit\Framework\TestCase;

final class MilestoneCreatorTest extends TestCase
{
    /**
     * @dataProvider getTestData
     */
    public function testCreate(array $data, array $expected): void
    {
        $milestone_creator = new MilestoneCreator();
        $result = $milestone_creator->create($data['issues'], $data['data'], $data['progress']);

        $this->assertEquals($expected, $result);
    }

    public function getTestData(): array
    {
        return [
            [
                [
                    'issues' => [
                        'queued' => [],
                        'active' => [],
                        'completed' => []
                    ],
                    'data' => [
                        'html_url' => 'html_url.com',
                        'title' => 'title'
                    ],
                    'progress' => [
                        'total' => 100,
                        'complete' => 25,
                        'remaining' => 75,
                        'percent' => 25
                    ]
                ],
                [
                    'milestone' => 'title',
                    'url' => 'html_url.com',
                    'progress' => [
                        'total' => 100,
                        'complete' => 25,
                        'remaining' => 75,
                        'percent' => 25
                    ],
                    'queued' => [],
                    'active' => [],
                    'completed' => []
                ]
            ]
        ];
    }
}
