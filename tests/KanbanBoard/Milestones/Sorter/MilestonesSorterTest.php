<?php
declare(strict_types=1);

namespace App\Tests\KanbanBoard\Milestones\Sorter;

use App\KanbanBoard\Milestones\Sorter\MilestonesSorter;
use PHPUnit\Framework\TestCase;

final class MilestonesSorterTest extends TestCase
{
    /**
     * @dataProvider getTestData
     */
    public function testSort(array $data, $expected): void
    {
        $milestones_sorter = new MilestonesSorter();
        $milestones_sorter->sort($data);

        $this->assertEquals($expected, $data);
    }

    public function getTestData(): array
    {
        return [
            [
                [
                    ['milestone' => '4.3.1'],
                    ['milestone' => '2.10.0'],
                    ['milestone' => '4.4.0'],
                    ['milestone' => '2.9.4'],
                    ['milestone' => '5.0.0'],
                    ['milestone' => '3.0.0']
                ],
                [
                    ['milestone' => '2.9.4'],
                    ['milestone' => '2.10.0'],
                    ['milestone' => '3.0.0'],
                    ['milestone' => '4.3.1'],
                    ['milestone' => '4.4.0'],
                    ['milestone' => '5.0.0']
                ]
            ]
        ];
    }
}
