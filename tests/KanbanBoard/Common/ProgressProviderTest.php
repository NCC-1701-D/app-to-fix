<?php
declare(strict_types=1);

namespace App\Tests\KanbanBoard\Common;

use App\KanbanBoard\Common\ProgressProvider;
use PHPUnit\Framework\TestCase;

final class ProgressProviderTest extends TestCase
{
    /**
     * @dataProvider getTestData
     */
    public function testGetProgress(array $data, array $expected): void
    {
        $progress_provider = new ProgressProvider();
        $progress = $progress_provider->getProgress($data['complete'], $data['remaining']);

        $this->assertEquals($expected, $progress);
    }

    public function getTestData(): array
    {
        return [
            [
                [
                    'complete' => 25,
                    'remaining' => 75
                ],
                [
                    'total' => 100,
                    'complete' => 25,
                    'remaining' => 75,
                    'percent' => 25
                ]
            ]
        ];
    }
}
