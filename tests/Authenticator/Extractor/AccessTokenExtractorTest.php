<?php
declare(strict_types=1);

namespace App\Tests\Authenticator\Extractor;

use App\Authenticator\Extractor\AccessTokenExtractor;
use PHPUnit\Framework\TestCase;

final class AccessTokenExtractorTest extends TestCase
{
    /**
     * @dataProvider getTestData
     */
    public function testGetAccessToken(string $result, string $expected): void
    {
        $access_token_extractor = new AccessTokenExtractor();
        $access_token = $access_token_extractor->getAccessToken($result);

        $this->assertEquals($expected, $access_token);
    }

    public function getTestData(): array
    {
        return [
            [
                'access_token=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&scope=repo&token_type=bearer',
                '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'
            ],
        ];
    }
}
