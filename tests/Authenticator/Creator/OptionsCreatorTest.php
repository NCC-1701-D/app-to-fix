<?php
declare(strict_types=1);

namespace App\Tests\Authentication\Creator;

use App\Authenticator\Creator\OptionsCreator;
use PHPUnit\Framework\TestCase;

final class OptionsCreatorTest extends TestCase
{
    /**
     * @dataProvider getTestData
     */
    public function testCreate(array $data, array $expected): void
    {
        $options_creator = new OptionsCreator();
        $options = $options_creator->create($data['code'], $data['client_id'], $data['client_secret']);

        $this->assertEquals($expected, $options);
    }

    public function getTestData(): array
    {
        return [
            [
                [
                    'code' => 'code',
                    'client_id' => 'client_id',
                    'client_secret' => 'client_secret',
                ],
                [
                    'http' => [
                        'method' => 'POST',
                        'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                        'content' => http_build_query(
                            [
                                'code' => 'code',
                                'state' => 'LKHYgbn776tgubkjhk',
                                'client_id' => 'client_id',
                                'client_secret' => 'client_secret'
                            ]
                        ),
                    ],
                ]
            ]
        ];
    }
}
