<?php
declare(strict_types=1);

namespace App\Authenticator\Extractor;

interface AccessTokenExtractorInterface
{
    public function getAccessToken(string $result): string|null;
}
