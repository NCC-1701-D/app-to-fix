<?php
declare(strict_types=1);

namespace App\Authenticator\Extractor;

final class AccessTokenExtractor implements AccessTokenExtractorInterface
{
    public function getAccessToken(string $result): string|null
    {
        $result = explode('=', explode('&', $result)[0]);
        array_shift($result);
        return array_shift($result);
    }
}
