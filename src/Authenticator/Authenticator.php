<?php
declare(strict_types=1);

namespace App\Authenticator;

use App\Authenticator\Creator\OptionsCreatorInterface;
use App\Authenticator\Extractor\AccessTokenExtractorInterface;
use App\Authenticator\Exception\AuthenticationException;

final class Authenticator implements AuthenticatorInterface
{
    private string $client_id;
    private string $client_secret;
    private OptionsCreatorInterface $options_creator;
    private AccessTokenExtractorInterface $access_token_extractor;

    public function __construct(
        string $client_id,
        string $client_secret,
        OptionsCreatorInterface $options_creator,
        AccessTokenExtractorInterface $access_token_extractor
    ) {
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->options_creator = $options_creator;
        $this->access_token_extractor = $access_token_extractor;
    }

    public function login(): string
    {
        session_start();
        $token = null;
        if (!empty($_SESSION['gh-token'])) {
            $token = $_SESSION['gh-token'];
        } elseif (array_key_exists('code', $_GET)) {
            $_SESSION['redirected'] = false;
            $token = $this->returnsFromGithub($_GET['code']);
        } else {
            $_SESSION['redirected'] = true;
            $this->redirectToGithub();
        }
        $_SESSION['gh-token'] = $token;

        return $token;
    }

    private function redirectToGithub(): void
    {
        header("Location: https://github.com/login/oauth/authorize?client_id={$this->client_id}"
            . '&scope=repo&state=LKHYgbn776tgubkjhk');
    }

    private function returnsFromGithub(string $code): string
    {
        $url = 'https://github.com/login/oauth/access_token';
        $options = $this->options_creator->create($code, $this->client_id, $this->client_secret);
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

        if ($result === false) {
            throw new AuthenticationException('Authentication failed');
        }

        $access_token = $this->access_token_extractor->getAccessToken($result);

        if ($access_token === null) {
            throw new AuthenticationException('Authentication failed');
        }

        return $access_token;
    }
}
