<?php
declare(strict_types=1);

namespace App\Authenticator\Exception;

final class AuthenticationException extends \Exception
{
}
