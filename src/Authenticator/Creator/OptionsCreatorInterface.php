<?php
declare(strict_types=1);

namespace App\Authenticator\Creator;

interface OptionsCreatorInterface
{
    public function create(string $code, string $client_id, string $client_secret): array;
}
