<?php
declare(strict_types=1);

namespace App\Authenticator\Creator;

final class OptionsCreator implements OptionsCreatorInterface
{
    public function create(string $code, string $client_id, string $client_secret): array
    {
        return [
            'http' => [
                'method' => 'POST',
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'content' => http_build_query(
                    [
                        'code' => $code,
                        'state' => 'LKHYgbn776tgubkjhk',
                        'client_id' => $client_id,
                        'client_secret' => $client_secret
                    ]
                ),
            ],
        ];
    }
}
