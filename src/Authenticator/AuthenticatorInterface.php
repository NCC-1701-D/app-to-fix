<?php
declare(strict_types=1);

namespace App\Authenticator;

interface AuthenticatorInterface
{
    public function login(): string;
}
