<?php
declare(strict_types=1);

namespace App\Client;

interface GithubClientInterface
{
    public function authenticate(string $token): void;

    public function milestones(string $repository): array;

    public function issues(string $repository, string $milestone_id): array;
}
