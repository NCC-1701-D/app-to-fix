<?php
declare(strict_types=1);

namespace App\Client;

use Github\Api\Issue;
use Github\Client;

final class GithubClient implements GithubClientInterface
{
    private Client $client;
    private string $account;
    private Issue $issue_api;

    public function __construct(string $account, Client $client)
    {
        $this->account = $account;
        $this->client = $client;
        $this->issue_api = $client->api('issue');
    }

    public function authenticate(string $token): void
    {
        $this->client->authenticate($token, null, Client::AUTH_ACCESS_TOKEN);
    }

    public function milestones(string $repository): array
    {
        return $this->issue_api
            ->milestones()
            ->all($this->account, $repository);
    }

    public function issues(string $repository, string $milestone_id): array
    {
        return $this->issue_api->all(
            $this->account,
            $repository,
            [
                'milestone' => $milestone_id,
                'state' => 'all'
            ]
        );
    }
}
