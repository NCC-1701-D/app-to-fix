<?php
declare(strict_types=1);

namespace App;

use App\Authenticator\Authenticator;
use App\Authenticator\Creator\OptionsCreator;
use App\Authenticator\Extractor\AccessTokenExtractor;
use App\Client\GithubClient;
use App\KanbanBoard\Common\ProgressProvider;
use App\KanbanBoard\KanbanBoardProvider;
use App\KanbanBoard\Milestones\Creator\MilestoneCreator;
use App\KanbanBoard\Milestones\Issues\IssuesProvider;
use App\KanbanBoard\Milestones\Issues\Mapper\Matcher\LabelsMatcher;
use App\KanbanBoard\Milestones\Issues\Mapper\Parser\MarkdownParser;
use App\KanbanBoard\Milestones\Issues\Mapper\RawIssuesToIssuesMapper;
use App\KanbanBoard\Milestones\Issues\Mapper\Resolver\StateResolver;
use App\KanbanBoard\Milestones\Issues\Sorter\IssuesSorter;
use App\KanbanBoard\Milestones\MilestonesProvider;
use App\KanbanBoard\Milestones\Sorter\MilestonesSorter;
use App\Utilities\Config\Builder\ConfigReaderBuilder;
use App\Utilities\Config\ConfigReaderDirector;
use Github\Client;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\Dotenv\Dotenv;

final class Application
{
    public function handle(): array
    {
        $config_reader = (new ConfigReaderDirector(new ConfigReaderBuilder()))->build(new Dotenv());
        $repositories = explode('|', $config_reader->getEnv('GH_REPOSITORIES'));
        $client_id = $config_reader->getEnv('GH_CLIENT_ID');
        $client_secret = $config_reader->getEnv('GH_CLIENT_SECRET');
        $authenticator = new Authenticator(
            $client_id,
            $client_secret,
            new OptionsCreator(),
            new AccessTokenExtractor()
        );
        $token = $authenticator->login();
        $client = Client::createWithHttpClient(new GuzzleClient());
        $github = new GithubClient($config_reader->getEnv('GH_ACCOUNT'), $client);
        $github->authenticate($token);
        $progress_provider = new ProgressProvider();
        $board_provider = new KanbanBoardProvider(
            $repositories,
            new MilestonesProvider(
                $github,
                $progress_provider,
                new MilestoneCreator(),
                new IssuesProvider(
                    $github,
                    new IssuesSorter(),
                    new RawIssuesToIssuesMapper(
                        $progress_provider,
                        new LabelsMatcher(),
                        new StateResolver(),
                        new MarkdownParser(),
                        ['waiting-for-feedback']
                    )
                ),
                new MilestonesSorter()
            )
        );

        return $board_provider->getBoard();
    }
}
