<?php
declare(strict_types=1);

namespace App\Utilities\Logger;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

final class LoggerFactory implements LoggerFactoryInterface
{
    public function createLogger(): LoggerInterface
    {
        $stream = new RotatingFileHandler(__DIR__ . '/../../../var/logs/log', Logger::DEBUG);
        $stream->setFormatter(new LineFormatter(null, 'Y-m-d H:i:s'));
        $command_logger = new Logger('KanbanBoard');

        return $command_logger->pushHandler($stream);
    }
}
