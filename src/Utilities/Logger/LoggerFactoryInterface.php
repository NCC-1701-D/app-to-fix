<?php
declare(strict_types=1);

namespace App\Utilities\Logger;

use Psr\Log\LoggerInterface;

interface LoggerFactoryInterface
{
    public function createLogger(): LoggerInterface;
}
