<?php
declare(strict_types=1);

namespace App\Utilities\Config;

interface ConfigReaderInterface
{
    public function load(): void;

    public function getEnv(string $name, string|null $default = null): string;
}
