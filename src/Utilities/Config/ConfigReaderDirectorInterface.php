<?php
declare(strict_types=1);

namespace App\Utilities\Config;

use Symfony\Component\Dotenv\Dotenv;

interface ConfigReaderDirectorInterface
{
    public function build(Dotenv $dotenv): ConfigReaderInterface;
}
