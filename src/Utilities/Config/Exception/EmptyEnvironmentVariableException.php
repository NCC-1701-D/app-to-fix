<?php
declare(strict_types=1);

namespace App\Utilities\Config\Exception;

final class EmptyEnvironmentVariableException extends \Exception
{
}
