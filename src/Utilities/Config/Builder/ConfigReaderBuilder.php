<?php
declare(strict_types=1);

namespace App\Utilities\Config\Builder;

use App\Utilities\Config\ConfigReader;
use App\Utilities\Config\ConfigReaderInterface;
use Symfony\Component\Dotenv\Dotenv;

final class ConfigReaderBuilder implements ConfigReaderBuilderInterface
{
    private ConfigReaderInterface $config_reader;

    public function createReader(Dotenv $dotenv): self
    {
        $this->config_reader = new ConfigReader($dotenv);
        return $this;
    }

    public function load(): self
    {
        $this->config_reader->load();
        return $this;
    }

    public function getReader(): ConfigReaderInterface
    {
        return $this->config_reader;
    }
}
