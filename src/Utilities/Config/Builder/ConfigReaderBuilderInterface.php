<?php
declare(strict_types=1);

namespace App\Utilities\Config\Builder;

use App\Utilities\Config\ConfigReaderInterface;
use Symfony\Component\Dotenv\Dotenv;

interface ConfigReaderBuilderInterface
{
    public function createReader(Dotenv $dotenv): self;

    public function load(): self;

    public function getReader(): ConfigReaderInterface;
}
