<?php
declare(strict_types=1);

namespace App\Utilities\Config;

use App\Utilities\Config\Builder\ConfigReaderBuilderInterface;
use Symfony\Component\Dotenv\Dotenv;

final class ConfigReaderDirector implements ConfigReaderDirectorInterface
{
    private ConfigReaderBuilderInterface $config_reader_builder;

    public function __construct(ConfigReaderBuilderInterface $config_reader_builder)
    {
        $this->config_reader_builder = $config_reader_builder;
    }

    public function build(Dotenv $dotenv): ConfigReaderInterface
    {
        return $this->config_reader_builder
            ->createReader($dotenv)
            ->load()
            ->getReader();
    }
}
