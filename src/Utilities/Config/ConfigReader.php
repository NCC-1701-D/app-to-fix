<?php
declare(strict_types=1);

namespace App\Utilities\Config;

use App\Utilities\Config\Exception\EmptyEnvironmentVariableException;
use Symfony\Component\Dotenv\Dotenv;

final class ConfigReader implements ConfigReaderInterface
{
    private Dotenv $dotenv;

    public function __construct(Dotenv $dotenv)
    {
        $this->dotenv = $dotenv;
    }

    public function load(): void
    {
        $path = __DIR__ . '/../../../.env';

        if (file_exists($path)) {
            $this->dotenv->load($path);
        }
    }

    public function getEnv(string $name, string|null $default = null): string
    {
        if (empty($_ENV[$name])) {
            if ($default === null) {
                throw new EmptyEnvironmentVariableException("Environment variable {$name} not found or has no value.");
            }

            return $default;
        }

        return $_ENV[$name];
    }
}
