<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones;

interface MilestonesProviderInterface
{
    public function getMilestones(string $repository): array;
}
