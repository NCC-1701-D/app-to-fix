<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones;

use App\Client\GithubClientInterface;
use App\KanbanBoard\Common\ProgressProviderInterface;
use App\KanbanBoard\Milestones\Creator\MilestoneCreatorInterface;
use App\KanbanBoard\Milestones\Issues\IssuesProviderInterface;
use App\KanbanBoard\Milestones\Sorter\MilestonesSorterInterface;

final class MilestonesProvider implements MilestonesProviderInterface
{
    private GithubClientInterface $github;
    private ProgressProviderInterface $progress_provider;
    private MilestoneCreatorInterface $milestone_creator;
    private IssuesProviderInterface $issues_provider;
    private MilestonesSorterInterface $milestones_sorter;

    public function __construct(
        GithubClientInterface $github,
        ProgressProviderInterface $progress_provider,
        MilestoneCreatorInterface $milestone_creator,
        IssuesProviderInterface $issues_provider,
        MilestonesSorterInterface $milestones_sorter
    ) {
        $this->github = $github;
        $this->progress_provider = $progress_provider;
        $this->milestone_creator = $milestone_creator;
        $this->issues_provider = $issues_provider;
        $this->milestones_sorter = $milestones_sorter;
    }

    public function getMilestones(string $repository): array
    {
        $milestones = [];
        foreach ($this->github->milestones($repository) as $data) {
            $issues = $this->issues_provider->getIssues($repository, $data['number']);
            $progress = $this->progress_provider->getProgress($data['closed_issues'], $data['open_issues']);

            if ($progress['total'] === 0) {
                continue;
            }

            $milestones[] = $this->milestone_creator->create($issues, $data, $progress);
        }

        $this->milestones_sorter->sort($milestones);

        return $milestones;
    }
}
