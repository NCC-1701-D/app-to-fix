<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Creator;

interface MilestoneCreatorInterface
{
    public function create(array $issues, array $data, array $summary): array;
}
