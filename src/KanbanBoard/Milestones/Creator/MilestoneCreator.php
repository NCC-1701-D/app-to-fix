<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Creator;

final class MilestoneCreator implements MilestoneCreatorInterface
{
    public function create(array $issues, array $data, array $progress): array
    {
        return [
            'milestone' => $data['title'],
            'url' => $data['html_url'],
            'progress' => $progress,
            'queued' => $issues['queued'] ?? [],
            'active' => $issues['active'] ?? [],
            'completed' => $issues['completed'] ?? []
        ];
    }
}
