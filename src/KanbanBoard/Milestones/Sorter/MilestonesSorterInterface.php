<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Sorter;

interface MilestonesSorterInterface
{
    public function sort(array &$milestones): void;
}
