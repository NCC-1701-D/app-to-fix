<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Sorter;

use vierbergenlars\SemVer\version;

final class MilestonesSorter implements MilestonesSorterInterface
{
    public function sort(array &$milestones): void
    {
        usort($milestones, fn($a, $b) => (int)version::gte($a['milestone'], $b['milestone']));
    }
}
