<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Issues\Sorter;

interface IssuesSorterInterface
{
    public function sort(array &$board_issues): void;
}
