<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Issues\Sorter;

final class IssuesSorter implements IssuesSorterInterface
{
    public function sort(array &$issues): void
    {
        foreach (array_keys($issues) as $state) {
            usort(
                $issues[$state],
                fn($a, $b) => count($a['paused']) - count($b['paused']) === 0
                    ? strcmp($a['title'], $b['title'])
                    : count($a['paused']) - count($b['paused'])
            );
        }
    }
}
