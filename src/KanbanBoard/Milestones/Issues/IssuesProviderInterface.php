<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Issues;

interface IssuesProviderInterface
{
    public function getIssues(string $repository, int $milestone_id): array;
}
