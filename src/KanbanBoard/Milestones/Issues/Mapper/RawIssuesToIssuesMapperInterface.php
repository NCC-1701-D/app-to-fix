<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Issues\Mapper;

interface RawIssuesToIssuesMapperInterface
{
    public function map(array $issues): array;
}
