<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Issues\Mapper;

use App\KanbanBoard\Common\ProgressProviderInterface;
use App\KanbanBoard\Milestones\Issues\Mapper\Matcher\LabelsMatcherInterface;
use App\KanbanBoard\Milestones\Issues\Mapper\Parser\MarkdownParserInterface;
use App\KanbanBoard\Milestones\Issues\Mapper\Resolver\StateResolverInterface;

final class RawIssuesToIssuesMapper implements RawIssuesToIssuesMapperInterface
{
    private ProgressProviderInterface $progress_provider;
    private LabelsMatcherInterface $labels_matcher;
    private StateResolverInterface $state_resolver;
    private MarkdownParserInterface $markdown_parser;
    private array $paused_labels;

    public function __construct(
        ProgressProviderInterface $progress_provider,
        LabelsMatcherInterface $labels_matcher,
        StateResolverInterface $state_resolver,
        MarkdownParserInterface $markdown_parser,
        array $paused_labels = []
    ) {
        $this->progress_provider = $progress_provider;
        $this->labels_matcher = $labels_matcher;
        $this->state_resolver = $state_resolver;
        $this->markdown_parser = $markdown_parser;
        $this->paused_labels = $paused_labels;
    }

    public function map(array $raw_issues): array
    {
        $issues = [];

        foreach ($raw_issues as $raw_issue) {
            $issues[$this->state_resolver->resolve($raw_issue)][] = [
                'id' => $raw_issue['id'],
                'number' => $raw_issue['number'],
                'title' => $raw_issue['title'],
                'body' => $this->markdown_parser->parse($raw_issue['body']),
                'url' => $raw_issue['html_url'],
                'assignee' => !empty($raw_issue['assignee'])
                    ? $raw_issue['assignee']['avatar_url'] . '?s=16'
                    : null,
                'paused' => $this->labels_matcher->match($raw_issue, $this->paused_labels),
                'progress' => $this->progress_provider->getProgress(
                    substr_count(strtolower($raw_issue['body']), '[x]'),
                    substr_count(strtolower($raw_issue['body']), '[ ]')
                ),
                'closed' => $raw_issue['closed_at']
            ];
        }

        return $issues;
    }
}
