<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Issues\Mapper\Matcher;

interface LabelsMatcherInterface
{
    public function match(array $issue, array $needles): array;
}
