<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Issues\Mapper\Matcher;

final class LabelsMatcher implements LabelsMatcherInterface
{
    public function match(array $issue, array $needles): array
    {
        if (array_key_exists('labels', $issue)) {
            foreach ($issue['labels'] as $label) {
                if (in_array($label['name'], $needles)) {
                    return [$label['name']];
                }
            }
        }
        return [];
    }
}
