<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Issues\Mapper\Parser;

use Michelf\Markdown;

final class MarkdownParser implements MarkdownParserInterface
{
    public function parse(string $text): string
    {
        return Markdown::defaultTransform($text);
    }
}
