<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Issues\Mapper\Parser;

interface MarkdownParserInterface
{
    public function parse(string $text): string;
}
