<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Issues\Mapper\Resolver;

interface StateResolverInterface
{
    public function resolve(array $issue): string;
}
