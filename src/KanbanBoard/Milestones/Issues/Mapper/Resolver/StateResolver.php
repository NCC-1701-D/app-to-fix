<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Issues\Mapper\Resolver;

final class StateResolver implements StateResolverInterface
{
    public function resolve(array $issue): string
    {
        if ($issue['state'] === 'closed') {
            return IssueStateEnum::COMPLETED;
        }

        if (!empty($issue['assignee']) && count($issue['assignee']) > 0) {
            return IssueStateEnum::ACTIVE;
        }

        return IssueStateEnum::QUEUED;
    }
}
