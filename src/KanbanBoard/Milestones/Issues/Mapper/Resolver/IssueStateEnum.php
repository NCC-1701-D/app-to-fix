<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Issues\Mapper\Resolver;

final class IssueStateEnum
{
    const COMPLETED = 'completed';
    const ACTIVE = 'active';
    const QUEUED = 'queued';

    private function __construct()
    {
    }
}
