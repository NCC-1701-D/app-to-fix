<?php
declare(strict_types=1);

namespace App\KanbanBoard\Milestones\Issues;

use App\Client\GithubClientInterface;
use App\KanbanBoard\Milestones\Issues\Mapper\RawIssuesToIssuesMapperInterface;
use App\KanbanBoard\Milestones\Issues\Sorter\IssuesSorterInterface;

final class IssuesProvider implements IssuesProviderInterface
{
    private GithubClientInterface $github;
    private IssuesSorterInterface $issues_sorter;
    private RawIssuesToIssuesMapperInterface $raw_issues_to_issues_mapper;

    public function __construct(
        GithubClientInterface $github,
        IssuesSorterInterface $issues_sorter,
        RawIssuesToIssuesMapperInterface $raw_issues_to_issues_mapper
    ) {
        $this->github = $github;
        $this->issues_sorter = $issues_sorter;
        $this->raw_issues_to_issues_mapper = $raw_issues_to_issues_mapper;
    }

    public function getIssues(string $repository, int $milestone_id): array
    {
        $raw_issues = $this->github->issues($repository, (string)$milestone_id);
        $issues = $this->raw_issues_to_issues_mapper->map($raw_issues);
        $this->issues_sorter->sort($issues);

        return $issues;
    }
}
