<?php
declare(strict_types=1);

namespace App\KanbanBoard;

interface KanbanBoardProviderInterface
{
    public function getBoard(): array;
}
