<?php
declare(strict_types=1);

namespace App\KanbanBoard\Common;

interface ProgressProviderInterface
{
    public function getProgress(int $complete, int $remaining): array;
}
