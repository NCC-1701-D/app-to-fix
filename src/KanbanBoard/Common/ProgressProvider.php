<?php
declare(strict_types=1);

namespace App\KanbanBoard\Common;

final class ProgressProvider implements ProgressProviderInterface
{
    public function getProgress(int $complete, int $remaining): array
    {
        $total = $complete + $remaining;

        return [
            'total' => $total,
            'complete' => $complete,
            'remaining' => $remaining,
            'percent' => $total !== 0 ? $this->computePercent($complete, $total) : 0
        ];
    }

    private function computePercent(int $complete, int $total): float
    {
        return floor($complete / $total * 100);
    }
}
