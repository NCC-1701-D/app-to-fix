<?php
declare(strict_types=1);

namespace App\KanbanBoard;

use App\KanbanBoard\Milestones\MilestonesProviderInterface;

final class KanbanBoardProvider implements KanbanBoardProviderInterface
{
    private array $repositories;
    private MilestonesProviderInterface $milestones_provider;

    public function __construct(
        array $repositories,
        MilestonesProviderInterface $milestones_provider
    ) {
        $this->repositories = $repositories;
        $this->milestones_provider = $milestones_provider;
    }

    public function getBoard(): array
    {
        $board = [];
        foreach ($this->repositories as $repository) {
            $board[] = [
                'name' => $repository,
                'milestones' => $this->milestones_provider->getMilestones($repository)
            ];
        }

        return $board;
    }
}
